from dynamixel_sdk import *
from dataclasses import dataclass


@dataclass
class DXL:  # information About the motors used in this Skript (all motors must be identical)
    MODEL = "XC430-W240-T"
    SERIES = "X_SERIES"
    USB_PORT = "ttyUSB0"  # the usb port which is connected to the motor default: ttyUSB0
    BAUDRATE = 57600
    PROTOCOL_VERSION = 2.0
    DEVICE_NAME = f"/dev/{USB_PORT}"
    TORQUE_ENABLE = 1  # Value for enabling the torque
    TORQUE_DISABLE = 0  # Value for disabling the torque
    DXL_MOVING_STATUS_THRESHOLD = 20  # Dynamixel moving status threshold
    DEFAULT_MAX_VELOCITY = 306

    @dataclass
    class ADDR:  # contains the command table addresses of the X Series
        TORQUE_ENABLE = 64  # use this to enable and disable the movement of the motors
        GOAL_VELOCITY = 104  # sets how fast the motors will move
        PRESENT_VELOCITY = 128  # monitors the current velocity of the motor
        VELOCITY_LIMIT = 44  # absolute value of the highest velocity allowed
        GOAL_POSITION = 116  # sets the target position of the motor
        PRESENT_POSITION = 132  # monitors the current position of the motor
        PRESENT_LOAD = 126  # monitors the load the motors works against
        MOVING = 122  # monitors if the motor is moving or not
        PRESENT_INPUT_VOLTAGE = 144  # monitors the INPUT voltage of the motor
        GOAL_PWM = 100  # sets the target PWM
        PRESENT_PWM = 124  # monitors the current PWM
        MINIMUM_POSITION_VALUE = 0  # Refer to the Minimum Position Limit of product eManual
        MAXIMUM_POSITION_VALUE = 4095  # Refer to the Maximum Position Limit of product eManual
        OPERATING_MODE = 11  # the Operating Mode of the Motor (only writeable when torque is off)


@dataclass
class Motors:
    # motor IDs
    LEFT_FRONT = 241  # id of the front motor on the left side
    LEFT_REAR = 242  # id of the rear motor on the left side
    RIGHT_FRONT = 244  # id of the front motor on the right side
    RIGHT_REAR = 243  # id of the rear motor on the right side

    # motor groups
    RIGHT = (RIGHT_FRONT, RIGHT_REAR)  # all motors on the right side
    LEFT = (LEFT_FRONT, LEFT_REAR)  # all motors on the left side
    FRONT = (LEFT_FRONT, RIGHT_FRONT)  # all motors on the front side
    REAR = (LEFT_REAR, RIGHT_REAR)  # all motors on the right side
    LF_RR = (LEFT_FRONT, RIGHT_REAR)
    RF_LR = (RIGHT_FRONT, LEFT_REAR)
    ALL = (LEFT_FRONT, LEFT_REAR, RIGHT_FRONT, RIGHT_REAR)  # all motors


@dataclass
class OperatingMode:
    VELOCITY = 1
    POSITION = 3
    EXTENDED_POSITION = 4
    PWM = 16
    ALL = (VELOCITY, POSITION, EXTENDED_POSITION, PWM)


# turns the param into a tuple if it is not already iterable
def make_iterable(param):
    return param if hasattr(param, '__iter__') else (param,)


def enable_torque(motor_ids):
    return set_torque(motor_ids, DXL().TORQUE_ENABLE)


def disable_torque(motor_ids):
    return set_torque(motor_ids, DXL().TORQUE_DISABLE)


def set_torque(motor_ids, enabled):
    # make the motor ids iterable if they are not
    motor_ids = make_iterable(motor_ids)

    # log the beginning of setting the torque
    action: str = "enable" if enabled == DXL().TORQUE_ENABLE else "disable"
    plural: str = "motor" if len(motor_ids) == 1 else "motors"
    print("=" * 40)  # empty line
    print("Trying to %s the %s %s" % (action, plural, str(motor_ids)))

    # prepare the values which will be returned
    connected = []
    not_connected = []
    error_on_connecting = []

    # iterate over all motor_ids ant turn them on
    for motorId in motor_ids:
        # Enable Dynamixel Torque
        dxl_comm_result, dxl_error \
            = packet_handler.write1ByteTxRx(port_handler, motorId, DXL().ADDR().TORQUE_ENABLE, enabled)
        if dxl_comm_result != COMM_SUCCESS:  # unsuccessful
            print("Failed to %s %i because: %s" % (action, motorId, packet_handler.getTxRxResult(dxl_comm_result)))
            not_connected.append(motorId)
        elif dxl_error != 0:  # failure
            print("Error occurred on %s motor %i cause: %s" % (
                action, motorId, packet_handler.getRxPacketError(dxl_error)))
            error_on_connecting.append(motorId)
        else:  # success
            print("Motor %i successfully %sd" % (motorId, action))
            connected.append(motorId)
    # turn lists into tuples and return
    print("=" * 40)
    return tuple(connected), tuple(not_connected), tuple(error_on_connecting)


def set_operating_mode(motor_ids, operating_mode):
    print("=" * 40)
    print(f"Setting Operating mode on {motor_ids} to Velocity")
    for motor_id in motor_ids:
        dxl_comm_result, dxl_error = packet_handler.write1ByteTxRx(port_handler, motor_id, DXL().ADDR().OPERATING_MODE,
                                                                   operating_mode)
        if dxl_comm_result != COMM_SUCCESS:
            print("%s" % packet_handler.getTxRxResult(dxl_comm_result))
        elif dxl_error != 0:
            print("%s" % packet_handler.getRxPacketError(dxl_error))
        else:
            print(f"Successfully set Operating Mode to Velocity on Motor {motor_id}")
    print("=" * 40)


def get_max_velocity(motor_ids):
    motor_ids = make_iterable(motor_ids)
    result = []
    for motor_id in motor_ids:
        max_velocity, dxl_comm_result, dxl_error = packet_handler.read4ByteTxRx(port_handler, motor_id,
                                                                                DXL().ADDR().VELOCITY_LIMIT)
        if dxl_comm_result != COMM_SUCCESS or dxl_error != 0:
            result.append(DXL().DEFAULT_MAX_VELOCITY)
        else:
            result.append(max_velocity)

    # if different velocity limits are set -> return the minimum
    if len(set(result)) > 1:
        print(f"### !!! Motors have different speed limits: {set(result)}. There for {min(result)} is used as limit")
    return min(result)


def adapt_velocity(velocity):
    max_velocity = get_max_velocity(Motors().ALL)
    if abs(velocity) > max_velocity:
        velocity = max_velocity if velocity >= 0 else -max_velocity
    return velocity


def turn_motor(motor_ids, velocity):
    velocity = adapt_velocity(velocity)
    motor_ids = make_iterable(motor_ids)
    for motor_id in motor_ids:
        packet_handler.write4ByteTxRx(port_handler, motor_id, DXL().ADDR().GOAL_VELOCITY,
                                      velocity if motor_id in Motors().LEFT else -velocity)


def move_forward(velocity: int):
    turn_motor(Motors().ALL, velocity)
    input("Moving forward - Press enter")


def move_backward(velocity: int):
    turn_motor(Motors().ALL, -velocity)
    input("Moving backward - Press enter")


def move_to_right(velocity):
    turn_motor(Motors().LF_RR, velocity)
    turn_motor(Motors().RF_LR, -velocity)
    input("Moving right - Press enter")


def move_to_left(velocity):
    turn_motor(Motors().LF_RR, -velocity)
    turn_motor(Motors().RF_LR, velocity)
    input("Moving left - Press enter")


def move_forward_right(velocity):
    turn_motor(Motors().LF_RR, velocity)
    stop(Motors().RF_LR)
    input("Moving forward right - Press enter")


def move_forward_left(velocity):
    turn_motor(Motors().RF_LR, velocity)
    stop(Motors().LF_RR)
    input("Moving forward left - Press enter")


def move_backward_right(velocity):
    turn_motor(Motors().RF_LR, -velocity)
    stop(Motors().LF_RR)
    input("Moving backward right - Press enter")


def move_backward_left(velocity):
    turn_motor(Motors().LF_RR, -velocity)
    stop(Motors().RF_LR)
    input("Moving backward left - Press enter")


def turn_right_forward(velocity):
    turn_motor(Motors().LEFT, velocity)
    stop(Motors.RIGHT)
    input("Turning right forward - Press Enter")


def turn_right_backward(velocity):
    turn_motor(Motors().LEFT, -velocity)
    stop(Motors.RIGHT)
    input("Turning right backward - Press Enter")


def turn_left_forward(velocity):
    turn_motor(Motors().RIGHT, velocity)
    stop(Motors.LEFT)
    input("Turning left forward - Press Enter")


def turn_left_backward(velocity):
    turn_motor(Motors().RIGHT, -velocity)
    stop(Motors.LEFT)
    input("Turning left backward - Press Enter")


def rotate_clockwise(velocity):
    turn_motor(Motors().LEFT, velocity)
    turn_motor(Motors().RIGHT, -velocity)
    input("rotating clockwise - Press Enter")


def rotate_counter_clockwise(velocity):
    turn_motor(Motors().LEFT, -velocity)
    turn_motor(Motors().RIGHT, velocity)
    input("rotating counter clockwise - Press Enter")


def swing_clockwise_center_rear(velocity):
    turn_motor(Motors().LEFT_FRONT, velocity)
    turn_motor(Motors().RIGHT_FRONT, -velocity)
    stop(Motors.REAR)
    input("swing clockwise center rear")


def swing_counter_clockwise_center_rear(velocity):
    turn_motor(Motors().LEFT_FRONT, -velocity)
    turn_motor(Motors().RIGHT_FRONT, velocity)
    stop(Motors.REAR)
    input("swing counter clockwise center rear")


def swing_clockwise_center_front(velocity):
    turn_motor(Motors().LEFT_REAR, velocity)
    turn_motor(Motors().RIGHT_REAR, -velocity)
    stop(Motors.FRONT)
    input("swing clockwise center front")


def swing_counter_clockwise_center_front(velocity):
    turn_motor(Motors().LEFT_REAR, -velocity)
    turn_motor(Motors().RIGHT_REAR, velocity)
    stop(Motors.FRONT)
    input("swing counter clockwise center front")


def stop(motor_ids):
    turn_motor(motor_ids, 0)


# ********************************* CODE Here ***********************************
if __name__ == '__main__':
    # Initialize the PortHandler instance
    # Set the Port path
    # Get methods and members of PortHandler Linux
    port_handler = PortHandler(DXL().DEVICE_NAME)

    # Initialize the PacketHandler instance
    # Set the Protocol Version
    # Get the ProtocolPacketHandler
    packet_handler = PacketHandler(DXL().PROTOCOL_VERSION)

    # Open Port
    print("Trying to open a port")
    while not port_handler.openPort():
        print(f"Failed to connect to {DXL().DEVICE_NAME}. Trying again...")

    print("Connected")

    # set Baudrate
    print(f"Trying to set Baudrate to {DXL().BAUDRATE}")
    while not port_handler.setBaudRate(DXL().BAUDRATE):
        print("Failed setting the baudrate. Trying again...")
    else:
        print("Successfully set Baudrate")

    # disable torque to set operating mode
    disable_torque(Motors().ALL)

    # set operating mode on all motors to Velocity
    set_operating_mode(Motors().ALL, OperatingMode().VELOCITY)

    # enable torque
    enable_torque(Motors.ALL)

    vel = 200
    # drive
    move_forward(vel)
    move_backward(vel)
    move_to_right(vel)
    move_to_left(vel)

    move_forward_right(vel)
    move_forward_left(vel)
    move_backward_right(vel)
    move_backward_left(vel)

    turn_left_forward(vel)
    turn_right_forward(vel)
    turn_left_backward(vel)
    turn_right_backward(vel)
    rotate_clockwise(vel)
    rotate_counter_clockwise(vel)
    swing_clockwise_center_rear(vel)
    swing_counter_clockwise_center_rear(vel)
    swing_clockwise_center_front(vel)
    swing_counter_clockwise_center_front(vel)

    stop(Motors().ALL)

    # disable torque
    disable_torque(Motors.ALL)
