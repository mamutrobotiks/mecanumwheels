# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')
    dict = {
        1 : "Hello",
        "Hello": 1,
        print_hi("helle"): "Tschüss",
        "Tschüss": dict[print_hi("helle")],
        "hi": dict["pi"],
        "pi": dict["hi"]
    }

    print(dict[1], dict["Hello"], dict[print_hi("hello")], dict["pi"])
    print("\\")


# See PyCharm help at https://www.jetbrains.com/help/pycharm/
